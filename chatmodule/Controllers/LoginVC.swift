//
//  LoginVC.swift
//  qiscus-sdk-ios-sample-v2
//
//  Created by UziApel on 18/07/18.
//  Copyright © 2018 Qiscus Technology. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QiscusCore
class LoginVC: UIViewController {

    @IBOutlet weak var viewSubmit: UIView!
    @IBOutlet weak var tFName: UITextField!
    @IBOutlet weak var tFEmail: UITextField!
    
    var email = ""
    var displayName = ""
    var islogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print(email)
//        setupUI()
        
        
    }

    func setupUI(){
        self.navigationController?.isNavigationBarHidden = true
        
        self.tFName.delegate = self
        self.tFEmail.delegate = self
             
        self.tFName.setBottomBorder()
        self.tFEmail.setBottomBorder()
             
        self.tFName.addDoneButtonOnKeyboard()
        self.tFEmail.addDoneButtonOnKeyboard()
        
//        let tap = UITapGestureRecognizer(target: self, action:#selector(submitLogin))
//                self.viewSubmit.addGestureRecognizer(tap)
//
//               self.viewSubmit.layer.cornerRadius = 4
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.isNavigationBarHidden = false
        print("willappear")
        self.navigationController?.isNavigationBarHidden = false
        let app : Pengaturan = Pengaturan()
        let appID = app.appID
        
        
        
        QiscusCore.setup(WithAppID: appID)
//        submitLogin(email : email, displayName: displayName)
        if QiscusCore.isLogined {
            QiscusCore.connect()
            self.navigationController?.isNavigationBarHidden = false

            QiscusCore.shared.getRoom(withID: QismoPreference.instance.getRoomId(), onSuccess: { (room, comments) in
                let target = UIChatViewController()
                target.room = room
                self.navigationController?.pushViewController(target, animated: true)
            }) { (error) in
                print("error getRoom =\(error.message)")
            }
        }else {
            print("sampai disini gagal")
        }
    }
    
    func submitLogin(email: String, displayName: String, phone: String) {
        let app : Pengaturan = Pengaturan()
        let appID = app.appID
        
        
        
        QiscusCore.setup(WithAppID: appID)
//        submitLogin(email : email, displayName: displayName)
        if QiscusCore.isLogined {
            QiscusCore.connect()
            self.navigationController?.isNavigationBarHidden = false
            QiscusCore.shared.getRoom(withID: QismoPreference.instance.getRoomId(), onSuccess: { (room, comments) in
                let target = UIChatViewController()
                target.room = room
                self.islogin = false
                self.navigationController?.pushViewController(target, animated: true)
            }) { (error) in
                print("error getRoom =\(error.message)")
            }
        }else {
            self.islogin = true
        }
        if(email != "" && displayName != "" && phone != ""){
            let app : Pengaturan = Pengaturan()
            let appID = app.appID
                   QiscusCore.getNonce(onSuccess: { (result) in
                    self.login(nonce: result.nonce,email:email,displayName: displayName,appID : appID, phone: phone)
                   }, onError: { (error) in
                       print("error getNonce =\(error.message)")
                   })
               }
    }
    
    func login(nonce: String,email: String,displayName : String,appID : String, phone: String){
        let userID = email
        let name = displayName
//        let app : Pengaturan = Pengaturan()
        let appID = appID
        let jsonphone = ["phone" : phone]
        let params = ["app_id": appID,
                      "user_id": userID,
                      "name": name,
            "nonce": nonce,
            "user_properties" : jsonphone
        ] as [String : Any]
        
        Alamofire.request("https://qismo.qiscus.com/api/v2/qiscus/initiate_chat", method: .post,parameters: params, headers: nil).responseJSON { response in
            if response.result.value != nil {
                if response.result.isSuccess && ((response.response?.statusCode)! < 300) {
                    let dataJSON = JSON(response.result.value)
                    let room_id = dataJSON["data"]["customer_room"]["room_id"].intValue
                    let identityToken = dataJSON["data"]["identity_token"].string ?? ""
                    
                    QismoPreference.instance.setRoomId(roomID: "\(room_id)")
                    
                    let app : Pengaturan = Pengaturan()
                    app.validateUserToken(identityToken: identityToken,islogin:self.islogin)
                    
                    print("roomID ini =\(room_id)")
                } else {
                    //error
                    print("check response2 \(response.result.value)")
                }
            } else {
                //error
            }
        }
    }
    
}

extension LoginVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.tFName.setBottomColorGrey()
        self.tFEmail.setBottomColorGrey()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.tFName){
            self.tFName.setBottomGreen()
            self.tFEmail.setBottomColorGrey()
        }else{
            self.tFEmail.setBottomGreen()
            self.tFName.setBottomColorGrey()
        }
    }
    
}
